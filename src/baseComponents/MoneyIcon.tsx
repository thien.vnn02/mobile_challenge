import React from 'react';
import {StyleSheet, View} from 'react-native';
import {AppText} from 'mcomponents/index';
import {ColorDefault} from 'mtheme/ColorSchema/ColorDefault';

const MoneyIcon = () => {
  return (
    <View style={styles.container}>
      <AppText text="S$" style={styles.text} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: ColorDefault.Green167,
    paddingVertical: 3,
    paddingHorizontal: 12,
    borderRadius: 3,
    marginRight: 10,
  },
  text: {
    fontWeight: 'bold',
    fontSize: 13,
    color: ColorDefault.WhiteFFF,
  },
});
export default MoneyIcon;
