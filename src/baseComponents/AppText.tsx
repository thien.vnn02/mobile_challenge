import React, {FC} from 'react';
import {StyleSheet, Text, Platform} from 'react-native';

interface AppTextProps {
  text: string;
  style?: any;
}
const AppText: FC<AppTextProps> = ({text, style}) => {
  return (
    <Text style={style ? [styles.container, {...style}] : styles.container}>
      {text}
    </Text>
  );
};
const styles = StyleSheet.create({
  container: {
    fontWeight: 'normal',
    fontSize: 14,
    fontFamily: Platform.OS === 'android' ? 'AvenirNext' : 'Avenir Next',
  },
});
export default AppText;
