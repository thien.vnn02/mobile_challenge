import React, {FC} from 'react';
import ContentLoader, {Rect} from 'react-content-loader/native';

interface TitleLoader {
  width?: string;
  height?: string;
  widthBar?: string;
}

const TitleLoader: FC<TitleLoader> = ({
  width = '150',
  height = '50',
  widthBar = '140',
}) => {
  return (
    <ContentLoader
      speed={1}
      width={100}
      height={30}
      viewBox={`0 0 ${width} ${height}`}
      backgroundColor="#cbb3b3"
      foregroundColor="#ecebeb">
      <Rect x="5" y="-12" rx="3" ry="3" width={widthBar} height="44" />
    </ContentLoader>
  );
};
export default TitleLoader;
