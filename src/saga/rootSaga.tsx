import {all, takeEvery} from 'redux-saga/effects';
import {fetchCard, setSpendingLimit} from './cardSaga';

export const sagaActions = {
  GET_CARD_INFO: 'GET_CARD_INFO',
  SET_LIMIT: 'SET_LIMIT',
};

export function* rootSaga() {
  yield all([
    takeEvery(sagaActions.GET_CARD_INFO, fetchCard),
    takeEvery(sagaActions.SET_LIMIT, setSpendingLimit),
  ]);
}
