import {call, put} from 'redux-saga/effects';
import {apiBase, METHOD} from '../api';
import {API_CONST} from '../constants';
import {fetchData, setLimit} from '../features/card/cardSlice';

export function* fetchCard() {
  try {
    let result = yield call(() => apiBase(API_CONST.GET_INFO_CARD, METHOD.GET));
    yield put(fetchData(result[0]));
  } catch (err) {
    yield put({type: 'GET_CARD_INFO_FAILED'});
  }
}
export function* setSpendingLimit(limit: number) {
  yield put(setLimit(limit));
}
