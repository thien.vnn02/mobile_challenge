// @ts-ignore
export {default as Logo} from './Logo.svg';
export {default as CreditCard} from './pay.svg';
export {default as Up} from './Credit.svg'
export {default as Transfer} from './transfer.svg';
export {default as User} from './user.svg';
export {default as AddCard} from './AddCard.svg';
export {default as Banner} from './Banner.svg';
export {default as Eye} from './Eye.svg';
export {default as Freeze} from './Freeze.svg';
export {default as HideEye} from './HideEye.svg';
export {default as Insight} from './insight.svg';
export {default as LogoWithBranch} from './LogoWithName.svg';
export {default as Meter} from './Meter.svg';
export {default as SmallMeter} from './smallMeter.svg';
export {default as Visa} from './Visa.svg';
export {default as Back} from './back.svg';

