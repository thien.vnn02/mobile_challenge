import {ColorInterface} from 'mtype/Color';

export const ColorDefault: ColorInterface = {
  Black222: '#222222',
  Blue45F: '#25345F',
  Blue65A: '#0C365A',
  Gray014: '#00000014',
  Gray01F: '#0000001F',
  Gray029: '#00000029',
  Gray266: '#22222266',
  Gray5E5: '#E5E5E5',
  GrayDDD: '#DDDDDD',
  GrayEEE: '#EEEEEE',
  Green167: '#01D167',
  Green167opa10: 'rgba(1, 209, 103, 0.1)',
  Gray233: '#22222233',
  White000: '#00000000',
  WhiteF00: '#FFFFFF00',
  WhiteFFF: '#FFFFFF',
};
