export function IsValidateObject(object: object) {
  return object !== undefined && object !== null;
}
export const hasProperty = (object: any, property: string) => {
  return (
    IsValidateObject(object) &&
    Object.hasOwnProperty.call(object, property) &&
    IsValidateObject(object[property])
  );
};
export function isString(obj: object) {
  return obj !== undefined && obj !== null && obj.constructor == String;
}
const sliceString = (data: any) => {
  let newData = data.toString();
  let result = [];
  if (isString(newData) && newData != '0') {
    let len = newData.length;
    let mod = Math.floor(len / 3);
    for (let index = mod; index >= 0; index--) {
      let begin = -(index + 1) * 3;
      let end = len - index * 3;
      let subString3 = newData.slice(begin, end);
      if (subString3) {
        result.push(subString3);
      }
    }
  }
  return result;
};
const maskString = (result: any) => {
  let dataFormat = '';
  if (result.length > 0) {
    dataFormat = result[0];
    for (let i = 1; i < result.length; i++) {
      dataFormat += ',' + result[i];
    }
  }
  return dataFormat;
};
export const convertMaskString = (strNumber: any) => {
  return maskString(sliceString(strNumber));
};
export const removeMaskString = (maskNumber: any) => {
  let result = '';
  if (isString(maskNumber)) {
    result = maskNumber.replace(/[,]/g, '');
  }
  return result;
};
