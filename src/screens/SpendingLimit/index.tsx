import React from 'react';
import {SafeAreaView, StyleSheet, TouchableOpacity, View} from 'react-native';
import {ColorDefault} from 'mtheme/ColorSchema/ColorDefault';
import {Back, Logo} from 'mtheme/svg';
import SettingLimit from './components/SettingLimit';
import {AppText} from 'mcomponents/index';
import {useNavigation, useRoute} from '@react-navigation/native';
import {useDispatch} from 'react-redux';
import {sagaActions} from '../../saga/rootSaga';

const SpendingLimit = () => {
  const {goBack} = useNavigation();
  const {params} = useRoute();
  const dispatch = useDispatch();
  const pressBack = () => {
    dispatch({type: sagaActions.SET_LIMIT, limit: 0});
    // @ts-ignore
    params?.getBack();
    goBack();
  };

  return (
    <View style={styles.container}>
      <SafeAreaView />
      <View style={styles.headerContainer}>
        <TouchableOpacity onPress={pressBack}>
          <Back color={ColorDefault.WhiteFFF} />
        </TouchableOpacity>
        <Logo color={ColorDefault.Green167} />
      </View>
      <AppText text="Spending limit" style={styles.title} />
      <SettingLimit />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: ColorDefault.Blue65A,
    width: '100%',
    height: '100%',
  },
  headerContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    paddingHorizontal: 24,
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    lineHeight: 32,
    fontWeight: 'bold',
    color: ColorDefault.WhiteFFF,
    marginLeft: 24,
    marginTop: 20,
  },
});
export default SpendingLimit;
