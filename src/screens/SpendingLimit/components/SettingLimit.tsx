import React, {useState} from 'react';
import {
  FlatList,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {SmallMeter} from 'mtheme/svg';
import {ColorDefault} from 'mtheme/ColorSchema/ColorDefault';
import {AppText, MoneyIcon} from 'mcomponents/index';
import {convertMaskString, removeMaskString} from '../../../common/helper';
import {useNavigation} from "@react-navigation/native";
import {useDispatch} from "react-redux";
import {sagaActions} from "../../../saga/rootSaga";

const VALUE_DEFAULT = [5000, 10000, 20000];
const SettingLimit = () => {
  const [limit, setLimit] = useState(convertMaskString(''));
  const dispatch = useDispatch();
  const {goBack} = useNavigation();
  const onSubmit = () => {
    dispatch({
      type: sagaActions.SET_LIMIT,
      limit,
    });
    goBack();
  };
  const onChangeText = (text: string) => {
    let newText = removeMaskString(text);

    setLimit(convertMaskString(newText));
  };
  const onPressValue = (value: number) => {
    setLimit(convertMaskString(value.toString()));
  };

  const ListValue = () => {
    const value = ({item}: any) => {
      return (
        <TouchableOpacity
          onPress={() => onPressValue(item)}
          activeOpacity={0.5}
          style={{height: 40}}>
          <View style={styles.background} />
          <AppText
            text={`S$ ${convertMaskString(item.toString())}`}
            style={styles.price}
          />
        </TouchableOpacity>
      );
    };
    return (
      <View>
        <FlatList
          data={VALUE_DEFAULT}
          renderItem={value}
          keyExtractor={(_item: any, index) => index.toString()}
          showsHorizontalScrollIndicator={false}
          horizontal
          ItemSeparatorComponent={() => <View style={{width: 10}} />}
          bounces={false}
          scrollEnabled={false}
        />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.description}>
        <SmallMeter />
        <AppText
          text="Set a weekly debit card spending limit"
          style={styles.textDescription}
        />
      </View>
      <View style={styles.inputWrapper}>
        <MoneyIcon />
        <TextInput
          value={limit}
          onChangeText={onChangeText}
          style={styles.input}
          keyboardType="numeric"
        />
      </View>
      <AppText
        text="Here weekly means the last 7 days - not the calendar week"
        style={styles.placeholder}
      />
      <ListValue />
      <TouchableOpacity
        style={[
          styles.btnSubmit,
          {
            backgroundColor:
              limit === '' ? ColorDefault.Gray01F : ColorDefault.Green167,
          },
        ]}
        onPress={onSubmit}>
        <AppText text="Save" style={styles.submit} />
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: ColorDefault.WhiteFFF,
    height: '100%',
    marginTop: 40,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    paddingTop: 32,
    paddingHorizontal: 24,
  },
  description: {
    flexDirection: 'row',
  },
  textDescription: {
    marginLeft: 12,
  },
  inputWrapper: {
    marginTop: 17,
    paddingBottom: 6,
    flexDirection: 'row',
    borderBottomColor: ColorDefault.Gray5E5,
    borderBottomWidth: 1,
    alignItems: 'center',
    marginBottom: 12,
  },
  input: {
    marginLeft: 10,
    fontWeight: 'bold',
    fontSize: 24,
    width: '80%',
  },
  placeholder: {
    fontSize: 13,
    color: ColorDefault.Gray266,
    textAlign: 'center',
    marginBottom: 32,
  },
  price: {
    marginHorizontal: 24,
    marginVertical: 12,
    fontSize: 12,
    fontWeight: 'bold',
    color: ColorDefault.Green167,
  },
  background: {
    position: 'absolute',
    backgroundColor: ColorDefault.Green167,
    width: '100%',
    height: 40,
    opacity: 0.07,
    borderRadius: 4,
  },
  submit: {
    marginHorizontal: 132,
    marginVertical: 17,
    color: ColorDefault.WhiteFFF,
  },
  btnSubmit: {
    marginTop: 298,
    alignSelf: 'center',
    borderRadius: 30,
  },
});
export default SettingLimit;
