import React, {FC, useEffect, useState} from 'react';
import {StyleSheet, Switch, TouchableOpacity, View} from 'react-native';
import {AppText} from 'mcomponents/index';
import {AddCard, Banner, Freeze, Insight, Meter} from 'mtheme/svg';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {convertMaskString} from '../../../common/helper';
import {sagaActions} from '../../../saga/rootSaga';

interface ItemMenuProps {
  label: string;
  description: string;
  isToggle: boolean;
  icon: any;
}

const ItemMenu: FC<ItemMenuProps> = ({label, description, isToggle, icon}) => {
  const limit = useSelector((state: any) => state.cardReducer.limit);
  const dispatch = useDispatch();
  const [isOn, setOn] = useState<boolean>(
    limit > 0 && icon === 'meter' ? true : false,
  );

  useEffect(() => {
    console.log(limit);
    if (icon === 'meter') {
      if (limit > 0) {
        setOn(true);
      } else {
        setOn(false);
      }
    }
  }, [limit]);
  const {navigate} = useNavigation();
  const toggleSwitch = () => {
    setOn(!isOn);
    switch (icon) {
      case 'meter':
        if (limit > 0) {
          dispatch({type: sagaActions.SET_LIMIT, limit: 0});
        } else {
          navigate('SpendingLimit', {getBack: () => setOn(false)});
        }
        break;
      default:
        break;
    }
  };
  const Icon = () => {
    switch (icon) {
      case 'insight':
        return <Insight />;
      case 'meter':
        return <Meter />;
      case 'freeze':
        return <Freeze />;
      case 'addCard':
        return <AddCard />;
      case 'ban':
        return <Banner />;
      default:
        return <View />;
    }
  };
  return (
    <TouchableOpacity style={styles.container} disabled>
      <View style={{flexDirection: 'row', marginLeft: 24}}>
        <Icon />
        <View style={styles.content}>
          <AppText text={label} />
          <AppText
            text={
              icon === 'meter' && limit > 0
                ? `Your weekly spending limit is S$ ${convertMaskString(limit)}`
                : description
            }
            style={styles.description}
          />
        </View>
      </View>
      {isToggle && (
        <View style={{paddingRight: 10}}>
          <Switch
            style={{
              transform: [{scaleX: 0.6}, {scaleY: 0.6}],
            }}
            onValueChange={toggleSwitch}
            value={isOn}
          />
        </View>
      )}
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: 32,
    justifyContent: 'space-between',
    flex: 1,
  },
  content: {
    marginLeft: 12,
  },
  description: {
    lineHeight: 12,
    fontSize: 13,
    opacity: 0.4,
    marginTop: 5,
  },
});
export default ItemMenu;
