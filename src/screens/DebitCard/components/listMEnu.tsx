export const LIST_MENU = [
  {
    label: 'Top-up account',
    icon: 'insight',
    description: 'Deposit money to your account to use with card',
    isToggle: false,
  },
  {
    label: 'Weekly spending limit',
    icon: 'meter',
    description: "You haven't set any spending limit on card ",
    isToggle: true,
  },
  {
    label: 'Freeze card',
    icon: 'freeze',
    description: 'Your debit card is currently active',
    isToggle: true,
  },
  {
    label: 'Get a new card',
    icon: 'addCard',
    description: 'This deactivates your current debit card',
    isToggle: false,
  },
];
