import React, {FC, useRef} from 'react';
import {
  SafeAreaView,
  Animated,
  StyleSheet,
  View,
  Dimensions,
} from 'react-native';
import Card from './Card';
import {CardType} from 'mtype/Card';
import {LIST_MENU} from './listMEnu';
import ItemMenu from './ItemMenu';
import {ColorDefault} from 'mtheme/ColorSchema/ColorDefault';
import * as Progress from 'react-native-progress';
import {useSelector} from 'react-redux';
import {AppText} from 'mcomponents/index';

interface RenderMenuProps {
  card: CardType;
}

const {height, width} = Dimensions.get('window');

const RenderMenu: FC<RenderMenuProps> = ({card}) => {
  const scroll = useRef(new Animated.Value(0)).current;
  const {limit} = useSelector((state: any) => state.cardReducer);
  const translate = scroll.interpolate({
    inputRange: [0, 50],
    outputRange: [10, -10],
    extrapolate: 'clamp',
  });
  const {thruTime, cvv, number, cardHolderName, spent} = card;
  const SpendingProcess = () => {
    const getProcess = () => {
      return spent / limit;
    };
    const value = getProcess();

    return (
      <View style={styles.spendingContainer}>
        <View style={styles.containerText}>
          <AppText text={'Debit card spending limit'} style={{fontSize: 13}} />
          <View style={{flexDirection: 'row'}}>
            <AppText text={`$${spent}`} style={styles.spent} />
            <AppText text={`| $${limit}`} style={styles.limit} />
          </View>
        </View>
        <Progress.Bar
          useNativeDriver
          height={15}
          width={(90 * width) / 100}
          borderRadius={30}
          borderWidth={0}
          unfilledColor={ColorDefault.Green167opa10}
          color={ColorDefault.Green167}
          progress={value}
        />
      </View>
    );
  };
  return (
    <Animated.ScrollView
      style={styles.container}
      scrollEventThrottle={16}
      showsVerticalScrollIndicator={false}
      bounces={false}
      onScroll={Animated.event(
        [
          {
            nativeEvent: {
              contentOffset: {
                y: scroll,
              },
            },
          },
        ],
        {useNativeDriver: true},
      )}>
      <SafeAreaView style={{height: height / 5}} />
      <Animated.View
        style={[styles.behind, {transform: [{translateY: translate}]}]}
      />
      <Card name={cardHolderName} number={number} cvv={cvv} thru={thruTime} />
      {limit > 0 && <SpendingProcess />}
      {LIST_MENU.map((item: any, index: number) => {
        const {label, description, isToggle, icon} = item;
        return (
          <View key={index}>
            <ItemMenu
              label={label}
              description={description}
              isToggle={isToggle}
              icon={icon}
            />
          </View>
        );
      })}
      <View style={{height: limit > 0 ? 200 : 250}} />
    </Animated.ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    top: 50,
  },
  behind: {
    backgroundColor: ColorDefault.WhiteFFF,
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderTopRightRadius: 24,
    borderTopLeftRadius: 24,
    top: 300,
  },
  spendingContainer: {
    marginBottom: 32,
    marginHorizontal: 24,
  },
  containerText: {
    marginBottom: 6,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  spent: {
    fontSize: 13,
    fontWeight: 'bold',
    marginRight: 5,
    color: ColorDefault.Green167
  },
  limit: {
    fontSize: 13,
    fontWeight: 'bold',
    borderLeftWidth: 1,
    borderColor: ColorDefault.Gray266,
    color: ColorDefault.Gray266,
  }
});
export default RenderMenu;
