import React, {FC, useState} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {Eye, HideEye, LogoWithBranch, Visa} from 'mtheme/svg';
import {AppText, TitleLoader} from 'mcomponents/index';
import {ColorDefault} from 'mtheme/ColorSchema/ColorDefault';

interface CardProps {
  name: string;
  number: number;
  cvv: number;
  thru: string;
}

const Card: FC<CardProps> = ({name, number, cvv, thru}) => {
  const [isShow, setShow] = useState<boolean>(false);
  const NumberConvert = () => {
    const stringNumber = number.toString();
    const first = stringNumber.substring(0, 4);
    const second = stringNumber.substring(4, 8);
    const third = stringNumber.substring(8, 12);
    const forth = stringNumber.substring(12, 16);
    return (
      <View style={styles.number}>
        {[first, second, third, forth].map((item: string, index: number) => (
          <View key={item} style={styles.subNumber}>
            <AppText
              text={isShow || index === 3 ? item : '••••'}
              style={styles.subText}
            />
          </View>
        ))}
      </View>
    );
  };
  const MoreInfo = () => {
    return (
      <View style={styles.moreInfo}>
        <AppText text={`Thru: ${thru}`} style={styles.textMoreInfo} />
        <View style={{flexDirection: 'row'}}>
          <AppText
            text={`CVV: ${isShow ? cvv : ''}`}
            style={isShow ? styles.textMoreInfo : styles.isShowText}
          />
          {cvv === 0 ? (
            <TitleLoader widthBar={'50'} />
          ) : (
            <View style={styles.hideText}>
              {!isShow && <AppText text={'★★★'} style={styles.hideText} />}
            </View>
          )}
        </View>
      </View>
    );
  };
  const HideShowNumber = () => {
    return (
      <TouchableOpacity
        style={styles.containerShow}
        activeOpacity={1}
        onPress={() => setShow(!isShow)}>
        <View>{isShow ? <Eye /> : <HideEye />}</View>
        <AppText text={'Hide card number'} style={styles.textShow} />
      </TouchableOpacity>
    );
  };
  return (
    <View style={{marginVertical: 34}}>
      <HideShowNumber />
      <View style={styles.containerCard}>
        <LogoWithBranch style={styles.logo} />
        {name === '' ? (
          <TitleLoader width={'150'} height={'50'} />
        ) : (
          <AppText text={name} style={styles.name} />
        )}
        {number === 0 ? <TitleLoader widthBar={'240'} /> : <NumberConvert />}
        <MoreInfo />
        <Visa style={{alignSelf: 'flex-end', marginBottom: 24}} />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  containerCard: {
    paddingHorizontal: 24,
    backgroundColor: ColorDefault.Green167,
    marginHorizontal: 24,
    borderRadius: 6,
    width: '90%',
    alignSelf: 'center',
  },
  logo: {
    alignSelf: 'flex-end',
    marginTop: 24,
  },
  name: {
    fontSize: 22,
    fontWeight: 'bold',
    color: ColorDefault.WhiteFFF,
  },
  number: {
    flexDirection: 'row',
    marginTop: 24,
  },
  subNumber: {
    marginRight: 24,
    lineHeight: 19,
  },
  subText: {
    color: ColorDefault.WhiteFFF,
    letterSpacing: 3.6,
    fontWeight: 'bold',
  },
  moreInfo: {
    flexDirection: 'row',
    marginTop: 15,
  },
  textMoreInfo: {
    letterSpacing: 1.56,
    lineHeight: 24,
    fontSize: 13,
    color: ColorDefault.WhiteFFF,
    marginRight: 32,
    fontWeight: 'bold',
  },
  containerShow: {
    position: 'absolute',
    backgroundColor: ColorDefault.WhiteFFF,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    paddingTop: 8,
    paddingBottom: 20,
    paddingLeft: 8,
    top: -30,
    right: 0,
    marginRight: 20,
  },
  textShow: {
    marginLeft: 8,
    marginRight: 14,
    lineHeight: 20,
    fontSize: 12,
    color: ColorDefault.Green167,
    fontWeight: 'bold',
  },
  isShowText: {
    letterSpacing: 1.56,
    lineHeight: 24,
    fontSize: 13,
    color: ColorDefault.WhiteFFF,
    fontWeight: 'bold',
  },
  hideText: {
    fontWeight: 'bold',
    alignSelf: 'center',
    fontSize: 12,
    color: ColorDefault.WhiteFFF,
  },
});
export default Card;
