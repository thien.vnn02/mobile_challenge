import React, {useEffect} from 'react';
import {SafeAreaView, StatusBar, StyleSheet, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {sagaActions} from '../../saga/rootSaga';
import {ColorDefault} from 'mtheme/ColorSchema/ColorDefault';
import {Logo} from 'mtheme/svg';
import {AppText, MoneyIcon, TitleLoader} from 'mcomponents/index';
import {convertMaskString} from '../../common/helper';
import RenderMenu from './components/RenderMenu';

const DebitCard = () => {
  const dispatch = useDispatch();
  const {cardReducer} = useSelector(state => state);
  const {card} = cardReducer;
  const {type, availableBalance} = card;
  useEffect(() => {
    dispatch({type: sagaActions.GET_CARD_INFO});
  }, [dispatch]);

  const ContentHeader = () => {
    return (
      <View style={styles.containerHeader}>
        {type === '' ? (
          <TitleLoader width={'150'} height={'50'}/>
        ) : (
          <AppText text={`${type} Card`} style={styles.typeText} />
        )}
        <AppText text={'Available balance'} style={styles.balance} />
        <View style={styles.containerBalance}>
          <MoneyIcon />
          {availableBalance === 0 ? (
            <TitleLoader width={'150'} height={'50'}/>
          ) : (
            <AppText
              text={convertMaskString(availableBalance)}
              style={styles.typeText}
            />
          )}
        </View>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <StatusBar
        barStyle="light-content"
        translucent
        backgroundColor="transparent"
      />
      <SafeAreaView>
        <Logo color={ColorDefault.Green167} style={styles.logo} />
        <ContentHeader />
      </SafeAreaView>
      <RenderMenu card={card} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: ColorDefault.Blue65A,
    width: '100%',
    height: '100%',
  },
  logo: {
    alignSelf: 'flex-end',
    marginHorizontal: 24,
    marginTop: 16,
  },
  typeText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: ColorDefault.WhiteFFF,
  },
  containerHeader: {
    paddingLeft: 24,
  },
  balance: {
    marginTop: 24,
    color: ColorDefault.WhiteFFF,
  },
  containerBalance: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
export default DebitCard;
