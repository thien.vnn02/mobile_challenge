import {CardType} from 'mtype/Card';

export const cardState: CardType = {
  availableBalance: 0,
  cardHolderName: '',
  cvv: 0,
  moneySpending: 0,
  number: 0,
  thruTime: '',
  type: '',
  spent: 0
};

export const spendingLimit: number = 0;
