export interface ColorInterface {
  WhiteFFF: string;
  WhiteF00: string;
  White000: string;

  Gray01F: string;
  Gray029: string;
  GrayDDD: string;
  Gray014: string;
  Gray266: string;
  GrayEEE: string;
  Gray5E5: string;
  Gray233: string;

  Blue65A: string;
  Blue45F: string;

  Green167: string;
  Green167opa10: string;

  Black222: string;
}
