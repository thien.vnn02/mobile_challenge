export interface CardType {
  cardHolderName: string;
  thruTime: string;
  cvv: number;
  type: string;
  number: number;
  availableBalance: number;
  moneySpending: number;
  spent: number;
}
