import {configureStore} from '@reduxjs/toolkit';

import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import {rootSaga} from './saga/rootSaga';
import {cardSlice} from './features/card/cardSlice';

const sagaMiddleware = createSagaMiddleware();
const store = configureStore({
  reducer: {
    cardReducer: cardSlice.reducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({thunk: false}).prepend(sagaMiddleware).concat(logger),
});
sagaMiddleware.run(rootSaga);
export type AppDispatch = typeof store.dispatch;
export default store;
