// Declear const for status code for app.
export const STATUS_CODE_EXPRIED = 401;
export const STATUS_CODE_SUCCESS_200 = 200;
export const STATUS_CODE_SUCCESS_300 = 300;
export const STATUS_CODE_SEVER_ERROR = 500;
export const STATUS_CODE_UNKNOW = -1;
export const STATUS_CODE_SUCCESS_WITH_ERROR = 1002;
export const STATUS_CODE_SUCCESS_WITH_ABORT = 1003;
export const STATUS_CODE_TIMEOUT = 1001;
export const GET = 'GET';
export const POST = 'POST';
export const PUT = 'PUT';
export const DELETE = 'DELETE';
export const TIME_OUT = 500;
export const CONTENT_TYPE = 'content-type';
export const CONTENT_TYPE_FORM = 'application/x-www-form-urlencoded';
export const CONTENT_TYPE_JSON = 'application/json';

export const ACCEPT = 'Accept';
export const AUTHORIZATION = 'Authorization';
export const DEVICE_TOKEN = 'DeviceToken';
export const LANGUAGE = 'language';
export const X_AJAX_CALL = 'X-Ajax-Call';
export const HEADER = {
  CONTENT_TYPE,
  CONTENT_TYPE_FORM,
  CONTENT_TYPE_JSON,
};
export const METHOD = {
  GET,
  POST,
  PUT,
  DELETE,
};
