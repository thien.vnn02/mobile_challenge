import {
  METHOD,
  STATUS_CODE_SEVER_ERROR,
  STATUS_CODE_SUCCESS_200,
  STATUS_CODE_SUCCESS_300,
  STATUS_CODE_SUCCESS_WITH_ERROR,
  STATUS_CODE_TIMEOUT,
  TIME_OUT,
} from './constants';
import {getQueryString, setCustomError} from './utils';
import {helper} from '../common';
let _timeoutAPI = -1;
const timeout = (time: any, promise: any) => {
  return new Promise((resolve, reject) => {
    _timeoutAPI = setTimeout(() => {
      clearTimeout(_timeoutAPI);
      return reject(setCustomError(STATUS_CODE_TIMEOUT, 'Timeout'));
    }, time);
    promise.then(resolve, reject);
  });
};
const checkStatus = (response: any) => {
  console.log('checkStatus', response);
  if (
    response.status >= STATUS_CODE_SUCCESS_200 &&
    response.status < STATUS_CODE_SUCCESS_300
  ) {
    return response;
  }

  // Error with another exception 401, 403, 500... handle here
  return response
    .json()
    .then((json: any) => {
      console.log('checkStatus: ', json);
      const status = helper.hasProperty(response, 'status')
        ? response.status
        : -1;

      if (status === STATUS_CODE_SEVER_ERROR || status === -1) {
        return Promise.reject(
          setCustomError(
            STATUS_CODE_SEVER_ERROR,
            'Server has error ',
            'We cannot connect server',
          ),
        );
      }
      const error = helper.hasProperty(json, 'error') ? json.error : '';
      let msgError = '';
      if (helper.hasProperty(json, 'error_description')) {
        msgError += json.error_description;
      }
      if (helper.hasProperty(json, 'errorReason')) {
        msgError += json.errorReason;
      }
      return Promise.reject(setCustomError(response.status, error, msgError));
    })
    .catch((error: any) => {
      if (helper.hasProperty(error, 'code')) {
        return Promise.reject(error);
      } else {
        return Promise.reject(
          setCustomError(0, 'Server has problem. Please try again'),
        );
      }
    });
};
const parseJSON = (response: any) => {
  if (response.status === 204 || response.status === 205) {
    return null;
  }
  return response.json();
};
export const apiBase = (
  url: string,
  method: any,
  body?: string,
  options = {
    setTimeout: TIME_OUT,
    signal: null,
    navigation: null,
    isCustomToken: false,
    isOauthenToken: false,
    isUpload: false,
    customHeader: {},
    params: {},
  },
) => {
  return new Promise((resolve: any, reject: any) => {
    let headers: any = new Headers();
    if (!options.isUpload) {
      switch (method) {
        case METHOD.GET:
          if (helper.IsValidateObject(options.params)) {
            url += getQueryString(options.params);
          }
          break;
        case METHOD.POST:
          body = JSON.stringify(body);
          break;
        default:
          break;
      }
    }
    timeout(
      15000,
      fetch(url, {
        signal: options.signal,
        method,
        headers,
        body,
      }),
    )
      .then(checkStatus)
      .then(parseJSON)
      .then((json: any) => {
        clearTimeout(_timeoutAPI);
        if (helper.hasProperty(json, 'error')) {
          const {error} = json;
          if (error) {
            const _error = helper.hasProperty(json, 'error') ? json.error : '';
            const errorReason = helper.hasProperty(json, 'errorReason')
              ? json.errorReason
              : '';
            reject(
              setCustomError(
                STATUS_CODE_SUCCESS_WITH_ERROR,
                _error,
                errorReason,
              ),
            );
          }
        }
        resolve(json);
      })
      .catch((error: any) => {
        clearTimeout(_timeoutAPI);
        reject(error);
      });
  });
};
