import {STATUS_CODE_UNKNOW} from './constants';

export const setCustomError = (
  _code = STATUS_CODE_UNKNOW,
  _error = '',
  _reason = '',
) => {
  return {
    code: _code ?? STATUS_CODE_UNKNOW,
    error: _error,
    errorReason: _reason,
  };
};
export const appendHeader = (params: any, headers: any) => {
  const keys = Object.keys(params);
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    if (headers.has(key)) {
      headers.map[key] = params[key];
    } else {
      headers.append(key, params[key]);
    }
  }
};
export const getQueryString = (params: any) => {
  const esc = encodeURIComponent;
  return Object.keys(params)
    .map(k => `${esc(k)}=${esc(params[k])}`)
    .join('&');
};
