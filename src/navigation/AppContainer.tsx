import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../screens/Home';
import DebitCard from '../screens/DebitCard';
import Payments from '../screens/Payments';
import Credit from '../screens/Credit';
import Profile from '../screens/Profile';
import {CreditCard, Logo, Transfer, Up, User} from 'mtheme/svg';
import {ColorDefault} from 'mtheme/ColorSchema/ColorDefault';
import {createNativeStackNavigator} from 'react-native-screens/native-stack';
import SpendingLimit from '../screens/SpendingLimit';

const Tab = createBottomTabNavigator();

const DebitStack = createNativeStackNavigator();
const DebitContainer = () => {
  return (
    <DebitStack.Navigator screenOptions={{headerShown: false}}>
      <DebitStack.Screen name="Debit" component={DebitCard} />
      <DebitStack.Screen name="SpendingLimit" component={SpendingLimit} />
    </DebitStack.Navigator>
  );
};

const AppContainer = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({route}: any) => ({
          tabBarIcon: ({color}: any) => {
            switch (route.name) {
              case 'Home':
                return <Logo color={color} />;
              case 'DebitCard':
                return <CreditCard color={color} />;
              case 'Payments':
                return <Transfer color={color} />;
              case 'Credit':
                return <Up color={color} />;
              case 'Profile':
                return <User color={color} />;
              default:
                return null;
            }
          },
        })}
        tabBarOptions={{
          activeTintColor: ColorDefault.Green167,
          inactiveTintColor: ColorDefault.GrayDDD,
        }}>
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="DebitCard" component={DebitContainer} />
        <Tab.Screen name="Payments" component={Payments} />
        <Tab.Screen name="Credit" component={Credit} />
        <Tab.Screen name="Profile" component={Profile} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};
export default AppContainer;
