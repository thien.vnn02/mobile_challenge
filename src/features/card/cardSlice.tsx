import {createSlice} from '@reduxjs/toolkit';
import {cardState, spendingLimit} from '../../store/cardStore';
import {removeMaskString} from "../../common/helper";
export const cardSlice = createSlice({
  extraReducers: undefined,
  name: 'card',
  initialState: {
    card: cardState,
    limit: spendingLimit,
  },
  reducers: {
    fetchData: (state: any, action: any) => {
      return {
        ...state,
        card: action.payload,
      };
    },
    setLimit: (state: any, action: any) => {
      console.log(action.payload);
      return {
        ...state,
        limit: parseInt(removeMaskString(action.payload.limit)),
      };
    },
  },
});
export const {fetchData, setLimit} = cardSlice.actions;
