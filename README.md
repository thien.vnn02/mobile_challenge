# Mobile Challenge

## Clone project:

- Clone project with command:

```python
  git clone https://gitlab.com/thien.vnn02/mobile-challenge.git
```

## Set up project

- Install npm dependencies **`at project root folder`**:

```javascript
yarn
```

- Android:

Run:

```javascript
  yarn android // android project will auto link
  // this command make project run on android too
```

If it not work, Please open `android` folder with `Android Studio` to sync project by gradle, then clean project and rebuild

- IOS:

Run:

```javascript
  cd ios/ && pod install
```

If `pod` can not install, please new terminal tab and run this command:

```javascript
  sudo gem install cocoapods
```

Then, re-try with `pod install`

After `pod install` success please run this command to run project:

```javascript
  yarn ios
```



# App Overview
# I. Structure:

- App is using `Redux` to state management with middleware is `Redux-saga` and `Reddux-Toolkit`
- App is using `Navigation v5`
- App is using `Hooks`  
- App `START` at `bottom navigation`, so we have 5 main screens
    - `Home Screen`
        - You can found boots file at `./src/screens/Home/index.tsx`
        - Updating...
    - `Debit Card Screen`
        - You can found boots file at `./src/screens/DebitCard/index.tsx`
        - This screen is using to show card's information and menu
        - It has 2 components, and those are `Card's Details` (Card's info from api), `Set Limit Spending` (setting money spending limit in 7 days)
    - `Payments Screen`
        - You can found boots file at `./src/screens/Payments/index.tsx`
        - Updating...
    - `Credit Screen`
        - You can found boots file at `./src/screens/Credit/index.tsx`
        - Updating...
    - `Profile Screen`
        - You can found boots file at `./src/screens/Profile/index.tsx`
        - Using to show `User's info` and setting.
- `Data` from the `https://mockapi.io`    
- App using `Fastlane` to build file `APK` shipping to `Firebase Hosting`. You can download android app from `https://appdistribution.firebase.dev/i/f6cd156f8dce762b`
## II. Business Requirements 
- User can create new card, freeze card
- User can send money from card to user's account
- Review deactivated cards.
- Control spending by the way set limit spending and follow in app.
- Payments, credit and management account
